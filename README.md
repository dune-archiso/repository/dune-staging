# [`dune-staging`](https://gitlab.com/dune-archiso/repository/dune-staging) repository for Arch Linux

[![pipeline status](https://gitlab.com/dune-archiso/repository/dune-staging/badges/main/pipeline.svg)](https://gitlab.com/dune-archiso/repository/dune-staging/-/commits/main)
[![coverage report](https://gitlab.com/dune-archiso/repository/dune-staging/badges/main/coverage.svg)](https://gitlab.com/dune-archiso/repository/dune-staging/-/commits/main)

This is a third-party auto-updated repository with the following [tarballs](https://gitlab.com/dune-archiso/repository/dune-staging/-/raw/main/packages.x86_64). The DUNE core modules are here.

## Usage

### Add repository

1. Import GPG key from GPG servers.

```console
[user@hostname ~]$ sudo pacman-key --recv-keys 2403871B121BD8BB
[user@hostname ~]$ sudo pacman-key --finger 2403871B121BD8BB
[user@hostname ~]$ sudo pacman-key --lsign-key 2403871B121BD8BB
```

2. Add the following code to `/etc/pacman.conf`:

```toml
[dune-staging]
SigLevel = Required DatabaseOptional
Server = https://dune-archiso.gitlab.io/repository/dune-staging/$arch
```

### List packages

To show actual packages list:

```console
[user@hostname ~]$ pacman -Sl dune-staging
```

### Install packages

To install package:

```console
[user@hostname ~]$ sudo pacman -Syyu
[user@hostname ~]$ sudo pacman -S dune-{functions,typetree}
```

### `PKGBUILD`s from the [Arch User Repository](https://wiki.archlinux.org/title/Arch_User_Repository) 👀

- [`dune-logging`](https://aur.archlinux.org/pkgbase/dune-logging)
- [`dune-typetree`](https://aur.archlinux.org/packages/dune-typetree)
- [`dune-functions`](https://aur.archlinux.org/packages/dune-functions)
- [`dune-uggrid`](https://aur.archlinux.org/packages/dune-uggrid)
<!--
- [dune-typetree](https://gitlab.com/dune-archiso/pkgbuilds/dune/-/raw/main/PKGBUILDS/dune-typetree/README.md)
- [dune-functions](https://gitlab.com/dune-archiso/pkgbuilds/dune/-/raw/main/PKGBUILDS/dune-functions/README.md)
- [dune-logging](https://gitlab.com/dune-archiso/pkgbuilds/dune/-/raw/main/PKGBUILDS/dune-logging/README.md)
- [dune-uggrid](https://gitlab.com/dune-archiso/pkgbuilds/dune/-/raw/main/PKGBUILDS/dune-uggrid/README.md)
-->